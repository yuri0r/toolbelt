﻿# Description
anoyed from switching between tools? the amount of buttonpresses it takes to do so? does it feel like a problem that is solved in other games with simple selection wheels? yeah me too, so i made this mod. a simple selection wheel filled with all your tools just two buttonpresses away!

# Big 2.0 Update
    Pretty Animation and a new backdrop to improve visibility!
    don't like it? no problem as this update also comes with a configuration file now!
    just set "AnimationMilliseconds": 150 to skip the animation.
    set "UseBackdrop": false to keep the old style going.
    config also includes the option to select with the left thumb button instead of the right one
    and of course you can rebind the (keyboard) key wichs opens the menu.
    dont like editing the config file? i gotchu fam as this mod also supports Generic Mod Config Menu

# Usuage

## Mouse and Keyboard
press left alt once to open menu, select tool with mouse (both right and left click select the tool thats hoverd over).
press left alt again or clickanywhere but an tool will to close the menu again.

## Controller
left thumb stick click will open the menu. select an tool by moving the right thumb stick. item will be selected when thumb stick is centered again.
click left thumb stick again or b button will close the wheel without making a selection.

## Features:

- will adjust to the amount of tools in your inventory
- works with mouse+keyboard and controller
- rebind the key
- left or rightstick to select
- configurable what counts as a tool (specificly horse flute)
- configure wheter to hold down the menu key(only keyboard binding)
- configure swap style(swap item in inventory vs changing active hotpar to tool position)


## Planned features
- this far i consider this mod done.
- if you feel like having a suggestion or found a bug let me know .


## Known Issues

- ~~ have not tested with every item that is considered a tool, so some might not draw in the selection wheel. (name should still be displayed)~~ very generic draw method has been implemented and this shall never be problem.
- potential conflict with mods that also use the left thumbstick to trigger a menu
- you tell me


## Credits

    bblueberry, SwiftSails, Bifi and shekurika for giving advice on the SDV Discord thx a bunch!


Source Code
gitlab
